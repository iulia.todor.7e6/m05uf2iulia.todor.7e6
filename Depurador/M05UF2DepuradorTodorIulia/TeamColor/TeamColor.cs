﻿using System.Drawing;
/**Autor: Iulia Todor
 * Data: 14/12/2022
 * Description: Ens han demanat que solucionem els errors sintàctics i semàntics via depurador d' un programa que 
 * faci el següent: Given two colors returns the name of the team represented by the colors.
The correct values are:
Team1: white, green
Team2: white, blue,
Team3: white, brown
Team4: red, blue
Team5: red, black
Team6: green, red
.**/
public class TeamColor
{

    static void Main(string[] args)
    {
        string? color1 = Console.ReadLine();
        string? color2 = Console.ReadLine();

        if (color1 == "white")
        {
            if (color2 == "green") Console.WriteLine("Team1");
            else if (color2 == "blue") Console.WriteLine("Team2");
            else if (color2 == "brown") Console.Write("Team3");
        }

        else if (color1 == "red")
        {
            if (color2 == "blue")
            {
                Console.WriteLine("Team4");
            }
            else if (color2 == "black")
            {
                Console.WriteLine("Team5");
            }
        }

        else if (color1 == "green")
        {
            if (color2 == "red")
            {
                Console.WriteLine("Team6");
            }
        }

        else
        {
            Console.WriteLine("ERROR");
        }
    }

}

