﻿/**Autor: Iulia Todor
 * Data: 14/12/2022
 * Description: Copia aquest codi. Soluciona tots els errors i warnings, per tal que s'executi. Depura'l, 
 * pas a pas fins que arribis a una conclusió i contesta a les següents preguntes, mitjançant comentaris al 
 * final del main:
**/

public class Math
{
    static void Main(string[] args)
    {
        var result = 0.0;

        for (int i = 0; i < 1000000000; i++)
        {
            if (result > 100)
            {
                result = System.Math.Sqrt(result);
            }

            if (result < 0)
            {
                result += result * result;
            }

            result += 20.2;
        }

        Console.WriteLine("el resultat és" +  result);

        /**Quin valor té result quan la i == 1000?
        * 
        * Cuando i es igual a 1000, result es igual a 111,56230589874906
        * 
        El valor result és mai major que 110? I que 120?

        Puede ser mayor que 110, pero seguramente nunca sea mayor a 120
        result puede ser mayor que 100 en varios momentos, y esto es antes de que i aumente, ya que se le suma 20.2 antes de la siguiente iteración.
        La primera vez que result supera el 100 tiene el valor de 101. En la siguiente iteración entra al if, se le hace la raíz cuadrada y tiene 
        el valor 10.05. Como se le va sumando 20.2, la próxima vez que supera el 100 lo hace con el valor 111,05. A partir de aquí el incremento
        que va teniendo result es ínfimo, a tal punto que llevarían millones de iteraciones solo para llegar a 111,56. Tal vez en algún punto podría llegar
        a 120, pero sería cuando i equivalga a un número tan abismalmente largo que no pueda calcularse.
 
        **/

    }
}