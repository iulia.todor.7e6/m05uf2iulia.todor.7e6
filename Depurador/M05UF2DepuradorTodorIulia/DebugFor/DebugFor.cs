﻿/**Autor: Iulia Todor
 * Data: 14/12/2022
 * Description: Tenim aquest codi que dóna problemes, ho pots solucionar?
**/

public class DebugFor
{
    static void Main()
    {
        var numero1 = 54874518; //Dona problemes perquè el nombre es massa gran per a la memòria
        var numero2 = 25145;
        var result = 0;
        for (int i = 0; i < numero2; i++)
        {
            result += numero1;
        }

        Console.WriteLine("la multiplicació de {0} i {1} es {2}", numero1, numero2, result);
    }

}

